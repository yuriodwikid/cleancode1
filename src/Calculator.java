import java.util.*;

class Calculator {
    static Scanner input = new Scanner(System.in);

    static List<String> operasiAritmatika = new ArrayList<>();
    static StringBuilder tampungAngka = new StringBuilder();

    static double result = 0;

    static void parsingUserInputKeArraylist(String userInput) {
        for (int i = 0; i < userInput.length(); i++) {
            if(userInput.charAt(i) == '+' || userInput.charAt(i) == '-' ||  userInput.charAt(i) == '/' || userInput.charAt(i) == '*') {
                operasiAritmatika.add(tampungAngka.toString());
                operasiAritmatika.add(String.valueOf(userInput.charAt(i)));
                tampungAngka = new StringBuilder();
            } else {
                tampungAngka.append(userInput.charAt(i));
            }

            if (i == userInput.length() - 1) {
                operasiAritmatika.add(tampungAngka.toString());
                tampungAngka = new StringBuilder();
            }
        }
    }

    static double pertambahan(double result, int i) {
        return result + Double.parseDouble(operasiAritmatika.get(i));
    }

    static double perkurangan(double result, int i) {
        return result - Double.parseDouble(operasiAritmatika.get(i));
    }

    static double pembagian(double result, int i) {
        return result / Double.parseDouble(operasiAritmatika.get(i));
    }

    static double perkalian(double result, int i) {
        return result * Double.parseDouble(operasiAritmatika.get(i));
    }

    public static void main(String[] args) {
        System.out.print("Input number and operators that you want to calculate: ");

        try {
            String inputUser = input.next();

            parsingUserInputKeArraylist(inputUser);

            for (int i = 0; i < operasiAritmatika.size(); i++) {
                switch (operasiAritmatika.get(i)) {
                    case "+":
                        i++;
                        result = pertambahan(result, i);
                        break;
                    case "-":
                        i++;
                        result = perkurangan(result, i);
                        break;
                    case "/":
                        i++;
                        result = pembagian(result, i);
                        break;
                    case "*":
                        i++;
                        result = perkalian(result, i);
                        break;
                    default:
                        result = Double.parseDouble(operasiAritmatika.get(i));
                }
            }

            System.out.println("Result: " + result);

        } catch (InputMismatchException e) {
            System.out.println("Invalid input");
        }
    }
}
